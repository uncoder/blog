require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  
  describe "#markdown" do
    it "replace markdown syntax" do
      expect(helper.markdown("# heading")).to match('<h1>')
    end
  end

end
