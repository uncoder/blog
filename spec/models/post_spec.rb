require 'rails_helper'

RSpec.describe Post, type: :model do

  it "delete nested comment on destroy" do
    comment = FactoryGirl.create :comment
    expect {
      comment.post.destroy
    }.to change{ Comment.count }.by(-1)
  end

  describe "callbacks" do

    it "add mailer job to queue" do
      user = FactoryGirl.create :user
      expect {
        Post.create user: user, title: 'test post', body: 'text'
      }.to change{ ActiveJob::Base.queue_adapter.enqueued_jobs.count }.by(1)
    end

  end
  
end
