require 'rails_helper'

RSpec.describe Subscriber, type: :model do
  
  it "generate token after create" do
    subscriber = Subscriber.create email: 'subscriber@blog.com'
    expect(subscriber.token).not_to be_blank
  end

end
