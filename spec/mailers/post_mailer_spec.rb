require "rails_helper"

RSpec.describe PostMailer, type: :mailer do
  
  describe "post_mail" do
    
    let :post do
      mock_model Post, id: 1, title: 'post title', body: 'post body', user_id: 1
    end

    let :mail do
      PostMailer.post_mail post
    end

    before :each do
      @subscriber = FactoryGirl.create :subscriber
    end

    it 'assigns token' do
      expect(mail.body.encoded).to match(@subscriber.token)
    end

    it 'assigns title' do
      expect(mail.body.encoded).to match('post title')
    end

  end

end

