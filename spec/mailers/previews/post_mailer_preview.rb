# Preview all emails at http://localhost:3000/rails/mailers/post_mailer
class PostMailerPreview < ActionMailer::Preview

  def post_mail
    @post = Post.first
    PostMailer.post_mail(@post)
  end

end
