FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "username#{n}" }
    sequence(:email) { |n| "email_#{n}@blog.com" }
    password "123456789"
    password_confirmation "123456789"
  end

  factory :admin, parent: :user do
    admin true
  end
end
