FactoryGirl.define do
  factory :post do
    user
    title "Test post"
    body "Body for test post"
  end
end
