FactoryGirl.define do
  factory :subscriber do
    sequence(:email) { |n| "subscriber#{n}@blog.com" }    
  end
end
