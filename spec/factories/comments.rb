FactoryGirl.define do
  factory :comment do
    post
    sequence(:author) { |n| "author_#{n}" }
    sequence(:text) { |n| "text_#{n}" }
  end

end
