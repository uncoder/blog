require "spec_helper"

describe "posts/index.html.erb" do
  before :each do 
    @user = FactoryGirl.create :user
    assign(:posts, [
      stub_model(Post, title: "title", body: "body", user_id: @user.id)
    ])
  end

  it "Show manage links" do
    sign_in @user
    render
    expect(rendered).to match /Edit/
    expect(rendered).to match /Delete/
  end

  it "Hide manage links" do 
    render
    expect(rendered).not_to match /Edit/
    expect(rendered).not_to match /Delete/
  end

  it "Show manage links for admin" do
    @admin = FactoryGirl.create :admin
    sign_in @admin
    render
    expect(rendered).to match /Edit/
    expect(rendered).to match /Delete/
  end
end