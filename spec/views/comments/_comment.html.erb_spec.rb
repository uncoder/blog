require "spec_helper"

describe "comments/_comment.html.erb" do
  before :each do 
    @admin = FactoryGirl.create :admin
    @comment = stub_model(Comment, author: "author", text: "text", post_id: 1, id: 2)
  end

  it "Show manage links" do
    sign_in @admin
    render 'comments/comment', comment: @comment
    expect(rendered).to match /Delete/
  end

  it "Hide manage links" do 
    render 'comments/comment', comment: @comment
    expect(rendered).not_to match /Delete/
  end
end