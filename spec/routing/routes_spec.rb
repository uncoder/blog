require 'rails_helper'

RSpec.describe "root route", type: :routing do
  it "routes / to posts#index" do
    expect(get: "/").to route_to(
      controller: "posts",
      action: "index"
    )
  end
end