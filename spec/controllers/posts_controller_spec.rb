require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  describe "GET #index" do
    it "respond successfuly" do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "get all posts" do
      user = FactoryGirl.create :user
      FactoryGirl.create :post, user: user
      get :index
      expect(assigns(:posts)).to eq(Post.all)
    end
  end

  describe "GET #new" do
    it "redirect to login for unauthorized users" do
      get :new
      expect(response).to redirect_to(controller: 'devise/sessions', action: :new)
    end

    it "respond successfuly for authorized users" do
      user = FactoryGirl.create :user
      sign_in user
      get :new
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
  end

  describe "POST #create" do
    it "redirect to login for unauthorized users" do
      post :create, post: "whatever"
      expect(response).to redirect_to(controller: 'devise/sessions', action: :new)
    end

    it "create post" do
      user = FactoryGirl.create :user
      sign_in user
      expect{ 
        post :create, post: { title: 'Test title', body: 'Test post body' }
      }.to change{ Post.all.count }.by(1)
      expect(Post.last.user).to eq(user)
    end
  end

  
  describe "Manage posts" do
    before :each do 
      @user = FactoryGirl.create :user
      @post = FactoryGirl.create :post, user: @user
    end

    describe "GET #edit" do
      it "get post for edit" do
        sign_in @user
        get :edit, id: @post.id
        expect(assigns(:post)).to eq(@post)
      end

      it "respond succeffully for post owner" do
        sign_in @user
        get :edit, id: @post.id
        expect(response).to be_success
      end

      it "restrict access for guests" do
        get :edit, id: @post.id
        expect(response).to redirect_to(controller: 'devise/sessions', action: :new)
      end

      it "restrict access for other users" do
        user = FactoryGirl.create :user, email: 'other_user@blog.com'
        sign_in user
        expect {
          get :edit, id: @post.id
        }.to raise_error(CanCan::AccessDenied)
      end
    end

    describe "Put #update" do
      it "update post" do
        sign_in @user
        expect {
          put :update, id: @post.id, post: FactoryGirl.attributes_for(:post, title: "new-title")
        }.to change{ @post.reload.title }.to('new-title')
      end

      it "restrict access for other users" do
        user = FactoryGirl.create :user, email: 'other_user@blog.com'
        sign_in user
        expect {
          put :update, id: @post.id, post: FactoryGirl.attributes_for(:post, title: "new-title")
        }.to raise_error(CanCan::AccessDenied)
      end
    end

    describe "DELETE #destroy" do
      before :each do
        sign_in @user
      end

      it "delete post" do
        expect{ 
          delete :destroy, id: @post.id
        }.to change{ Post.all.count }.by(-1)
      end

      it "redirect to posts list" do 
        delete :destroy, id: @post.id
        expect(response).to redirect_to(posts_path)
      end
    end

    describe "GET #show" do
      it "respond successfuly" do
        get :show, id: @post.id
        expect(response).to be_success
      end

      it "get post" do
        get :show, id: @post.id
        expect(assigns[:post]).to eq(@post)
      end
    end
  end

end



