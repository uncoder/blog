require 'rails_helper'

RSpec.describe SubscribersController, type: :controller do

  describe "GET #index" do
    it "raise error if not admin" do
      expect {
        get :index
      }.to raise_error(CanCan::AccessDenied)
    end
  end

  describe "POST #new" do
    it "create new subscriber" do
      expect{
        post :create, subscriber: { email: 's1@blog.com' }, format: :js
      }.to change{ Subscriber.count }.by(1)
    end
  end

  describe "get #unsubscribe" do
    before :each do
      @subscriber = FactoryGirl.create :subscriber
    end

    it "delete subscriber by token" do
      expect{
        get :unsubscribe, id: @subscriber.token
      }.to change{ Subscriber.count }.by(-1)
    end 

    it "respond successfuly" do
      get :unsubscribe, id: @subscriber.token
      expect(response).to be_success
    end
  end

end
