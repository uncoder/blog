require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  before :each do 
    @post = FactoryGirl.create :post
  end

  describe "POST #create" do
    it "create comment for post" do
      expect {
        post :create, post_id: @post, comment: { author: 'test-author', text: 'test-text' }
      }.to change{ @post.comments.count }.by(1)
    end

    it "create comment with user username" do
      user = FactoryGirl.create :user, username: 'test-user'
      sign_in user
      post :create, post_id: @post, comment: { author: 'test-author', text: 'test-text' }
      expect(@post.comments.last.author).to eq('test-user')
    end

    it "create comment with author name" do
      post :create, post_id: @post, comment: { author: 'test-author', text: 'test-text' }
      expect(@post.comments.last.author).to eq('test-author')
    end
  end

  describe "DELETE #destroy" do
    before :each do 
      @admin = FactoryGirl.create :admin
      @comment = FactoryGirl.create :comment
    end

    it "destroy comment" do
      sign_in @admin
      expect{
        delete :destroy, post_id: @comment.post_id, id: @comment.id
      }.to change{ Comment.count }.by(-1)
    end

    it "do not allow to destroy" do
      expect{
        delete :destroy, post_id: @comment.post_id, id: @comment.id
      }.to raise_error(CanCan::AccessDenied)
    end

  end

end