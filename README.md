# Simple blog application #

### Requirements: ###
* ruby 2.2.1 (Project includes rvm scripts)
* postgresql
* redis (required for sidekiq)

### Installation insctructions: ###
* Clone apllication source from git repository
* Create postgres role **blog** with create db privilegies or edit **database.yml** file
* Configure prefered mail delivery method in your environment configuration file
* Optional: configure RVM environment
* **bundle install**
* **bin/rake db:setup** to create databases and fill tables with test data
* **bin/rails s** to run rails server

### Seed data ###
After **db:setup** script execution you will have a few posts and users records
user1@blog.com, user2@blog.com and admin@blog.com
Password for all accounts is **123456789**

### Async Mailer: ###
Application supports asynchronous email delivery. ActiveJob + Sidekiq
You need to start mailer jobs backgroung processing with command:
```
bundle exec sidekiq -q mailers
```

### Testing ###
**bin/rspec** to run all specs

### Features ###
* Styled with Twitter Bootstrap
* Authorization
* Data management roles. (Users can manage their posts, Admin can manage all posts, comments and subscribers)
* Markdown syntax for posts body
* Comments for posts
* Subsribe to news
* Async email delivery
* SEO-friendly posts urls
* Test coverage