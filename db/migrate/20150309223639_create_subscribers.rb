class CreateSubscribers < ActiveRecord::Migration
  def change
    create_table :subscribers do |t|
      t.string :token
      t.string :email

      t.timestamps null: false
    end
  end
end
