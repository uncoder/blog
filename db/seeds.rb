titles = []
texts = []

titles << "Fictum Harpalos poscit antiqua"
texts << t = <<END
## Quid iacentem

Lorem markdownum data, potuit, progenies protinus conscia sacris in arcus
facibusque vertere; adeo. Tantus ubi referuntur ipsaque in ridet tua securaque
fuit, nihil tota Hiberis femina, vis adest Ceycis fortibus! Patria vestes
tantum, parantes murmure, tam Thermodontiaca suos lentae gravitate omnes
reperitur nequiret memorant Oceano, e. Furto *appellat*! Lux tigres signaque
quoque parenti, ex ad visa et fuso nec sublimis, aurumque.

- Ascendunt longa
- Ignaro animos
- Veras infames oracula
- Flumen est timor non se ad cum
- Tuis frondes Agenore invidiosus licebit memores opis
- Nyseides trepidare ponto

Flectere *multa superabit* clamare cacumine; loqui tura ortu, et harum exsul id
operitur longa; erat! Fieri undas venit reppulit; quod ultra oscula, sustinet
frequens vacarunt tuti spectavit illum suis evolvere ad manibus notis. Vidit
sunt adiere, recurvam [respicit in](http://www.raynelongboards.com/) medicamina
prohibes quid. Et templi fallere robora novi: virgo furores Actaeo custodia
vocari! In In duae erat famulis; nocendo pronaque; auras sic, mota fugientem
nocte Lyaeo ima?

## Sic litoris

Undis servatos meae, digna quid relatus dextra mente alvum nasci arripit
receptae an *Lycetum tristia*. Velut habenti; non erili *pollice Palati sano*
sitim domo vires *exigit non* palluerat.

Omnia bipennis sputantem me init timorem nitentem et sedes, quam terga accessi
et semel si humani ante Rhexenore turba. Alte *pressanda* tam laudatis linguae
suos *corpore* similis praevalidusque tantaque: tradita colebat ab nemus venti
acuta terque resistere ulnis. Infamis an venter cruentum verba debuerant
audistis torruit tellus promissaque deus montis a prosit urbs. Stet meis titulum
constitit utque hiatu munere nusquam Sarpedonis ferat congrediturque Aegeus
pressanda emisit terras turba formidatis inde novus primitias? Temporis quamvis
corneus iuvenali recepta minimo nec lanianda virum *securiferumque* madida et
vestro consedere dummodo.

1. Iam lanigerosve haec placuit ora tulit
2. Ante simillimus rapiunt esse humana de quod
3. An suppressit iacet vocalis totusque correptus luctus

Hausto inmutatque. Libandas ite nitidam cernis ad media, relicta adspiciens in
est quam haberet vincemur.

Noxque utrumque; Cnosius iam, se haec. Erat condar omnibus Nemeaea mei grege
arma vivus virgo, fateri subsedit isto ego.
END

titles << "Agendo curvum"
texts << t = <<END
## Ut huic neque Apolline audisse crimenque iungere

Lorem markdownum inquit, senior tantum; aequoris eundo quamvis belli; an quid;
ut nec totidemque ipse hunc? Aurigenae tot Polymestora vacuo, meritorum datis
ostendens ferunt mihique montis, sic. Victore scelus cumque, undas?

## Est quem

Partique Oetaeus diversas *paternam canit et* ripis arces terras: resilire
galeaque rite suo insistit loquendi regalia inluxisse. Contra et nomen
cadentibus, purpura hoc umquam potentia, per erubuit animum dubitare. Nautae
tenuerunt cum hoc sed ea et Dryantis, quid aurum. Pinetis pedum intervenit
litoream ille gerebat avus inque altis ruit bene deus tectis?

- Est quod
- Ore opperiuntur animumque locorum tantus illic
- Erit carpitur pisce
- Spissus coniugis sedes et stat
- Cultu pectora tenebras utilibus Proetus monendo centum
- Meque positoque occiduae visus in remos conplexa

## Coeperat nam est acerque simul et repleam

Matrem gulae. Idem orabam apertas considere spes penetratque, frontemque
sublimis, latent [infesto](http://reddit.com/r/thathappened) legebant visaque
derantque voce summis rescindere fingebam!

> Sedes **potuisset** ad possit. In vera, ullo Aeolidae erat Cressa posse
> distinctas utque duos sana fornace rudem, in nata, in. Fueris domito et
> contactu ultor fugit suarum! Arcadis nondum ille dicenti fluxit.

## Omnia non negare dignas nunc animans

Non vulgata terram tua mors nescio prendique rerum si, socios ei. Populus
nondum, quae bella timido membra caecamque *quaeras fontes* funere in Milon.
Minervae divitias Eumenides in veste fruentur [Achille
verumtamen](http://www.raynelongboards.com/) ipsa, gravitate stupet, ubi. Nondum
innixus illius iaculum Pandione soporem profani: mali quo animi, mora lentus
transformat litore ferax.

Nec colonos longis si creatus postquam aliter vectus levitate: sibi Musae sidera
videamus vidit. Gelida iphis saepius Perseus nostrumque Nec dedit professa
Caenea Peucetiosque tradere et quid. **Germanam ipse** Philomela caper nox per:
a ab orbi divesque, marinae? Ultima ureret mea rigidum oscula ut quo Cecropidis
bracchia fingit, contemnere numina maestam, cum.
END

titles << "Praecipitique illa poena quisquam quondam senex humano"
texts << t = <<END
## Saltumque adnuat ubicumque venenis palla

Lorem markdownum grave, aura ille quoque tradit hostes? Ora in, fusis quis arcem
inque foret timido habebas. Mille senemque quidem flamina, fessum domibus retro
licet senex aurea rima saltatibus amnis, omnia.

Editus o capax sed **nare** magis **mores paenituit** inmodico, medias superari
additis. Sis inde et herba, et neque notissima naufragus est, est domito.

## Sol sub expellere sanguine

Sonat canes simul, est, ut apte require angebar taedasque Thermodontiaco honores
quotiens. Fores fugae suisque formatum.

> Poma de corpore protinus. Me vix est figurae quem quondam dignus protecta
> detulit, relinquam Adonis poenas. Sed omnes ritibus et alter indoctum Terror
> cum veteris nostra, vero illis tuebere credita sparsitque taceam. Rationis
> crimina.

## Sacra delubraque tamen aequore Dryopen partimque placebimus

Sociorum nurus repetita crines. Deprensa meo *quos tamquam iamque*; ac sermo
ipse per sum est virentem: sede.

## Iactant non

Tenui vertere et caelum origo aurum palmis, non neque inmeritam, seque
abstitimus orbem, munus quid, soporis. Sed illi mori ramis in antiquum putares,
deae [abibo](http://en.wikipedia.org/wiki/Sterling_Archer) magni est. Tua stant,
ipse [obliquo lucis movetur](http://twitter.com/search?q=haskell) annis, verba
oscula pectore. Eurus naturale fulgure, populi mundi dictaque submissa verbere
incertam et sanguine nomine: **bellum o**. Vulnere rapidas, erat sim Achille
Minyae: munus comes ius seu litore nulla curru pecorumque murmur quoque nescimus
ipsa.

## Enim rupit neutrumque bellaque longa inque finiat

Vincere tauri inque filia, cum solebat pater modo ille et? Solet frigore, solvit
tecti exstant vetui Amphrysos cape poposcerat inplent dixit opaca, reliquit
tangeris. Sparsitque et Dianae natae exi. Corque meri ignibus [ventoque
magis](http://zeus.ugent.be/) Amphionis parente illi: qui qualia.

Noctis an intus herbas; de utque Iuno adspergine Interea petentem! [Iuncta
quo](http://zombo.com/) non stratis modo est cepi lacrimasque **dixit**, moenia
nudus. Recurvam si tenebat, umbrosa quae vitiaverit odio restabat, dedit:
fontibus.

Relictis feres **ad** totum mori cycnorum in nigra par contigit primum regi ore
stupuitque causa. Pylius **erat**; removit colle; dona mihi letique ductae
serpentis tamen simul; astu?
END

titles << "Sine nullo roganti est Hyperborea tamen errorem"
texts << t = <<END
## Nec scelus niveo

Lorem markdownum frondes teli resecuta et Fames lacrimoso auris: qui me stravit
dixit, solis. [In tuo](http://eelslap.com/), ferro.

1. Ille ante tulit possis et egit lacrimae
2. Minus supersunt aeterno
3. Me caelum iacuere

## Et ubi tremulae omnes

Tamen virili ense omnia; haec paenituisse vittae; decurrere certe femina,
reppulit. Fixit languescuntque *magis in* inter Stygii victrix, ulterius
deformes. Curvataque **at ille istas** videres servabant oceano albentibus animo
**morari** vestro claro et obortae dolebo, iam hiems sorbere.

1. Non Thisbe sacraque eo misit quemque monstravit
2. Forte in deae imis regnare Phoenissa silva
3. Summis non levavit
4. Mihi longis relictum audet
5. Parentem sacra notas aliter in captas
6. Populum hostis saepe cognita simul

Gremioque ramis praebentque me velut Cereris nequiquam *Phineus muneribusque*
iussam detrahis timore conveniunt corripit pelagi. Mea multorum. *Monte* Mota
leoni redunco astra videri vidit. Est illi meta **deae laeti**, probaverat:
postquam silices sentit Aeacidis namque acumina sequente ignoto, et. Nunc ora
illi semine dulce, spatium diriguit tulit.

## Ablatus eventu sit Thebae fuit ablatus cum

Adlevet medio ille patriae paravi. Pondere postquam utrumque Alexiroe patiar
minus, corripiunt gestet laevae ita inferior altus ad fidissima poscat, regis,
ab. *Esse* poenam violenta ex gloria ne copia atque unus partes: spoliata
Scythiae, tellusque ausus, reor. Oro oculosque memini. Troezena sermone proles
tuis quoque in ad anilia.

Quae iram durastis fulmen genetrix agrestes non opus sub aegram gessit; campos
illi. Populo non puer luctus [rostro](http://stoneship.org/) ut Melampus filia
ipse petens; fingit *flavae*; tecto. *Quae conantur* at additur, adit, quae rore
servabunt discedere et caelo spatiosa?

Habet **dixit Vix magnumque** quae. Medio et fecitque tenebras negat fameque:
dominique regnumque retia verso Nam, haut mihi arcet, animam. Derantque parenti
puerique exerces sumpta.
END

user1 = User.create email: 'user1@blog.com', username: 'user1', password: '123456789', password_confirmation: '123456789'
user2 = User.create email: 'user2@blog.com', username: 'user2', password: '123456789', password_confirmation: '123456789'
admin = User.create email: 'admin@blog.com', username: 'admin', password: '123456789', password_confirmation: '123456789', admin: true

3.times do |n|
  Post.create user_id: user2.id, title: titles[n], body: texts[n]
end

post1 = Post.create user_id: user1.id, title: titles[3], body: texts[3], comments: [
  Comment.new(author: 'author 1', text: 'first comment'),
  Comment.new(author: 'author 2', text: 'second comment')
]

Subscriber.create email: 'subscriber@blog.com'