class CommentsController < ApplicationController
  load_and_authorize_resource

  def create
    @post = Post.friendly.find params[:post_id]
    @comment = @post.comments.build comment_params
    @comment.author = current_user.username if user_signed_in?
    if @comment.save
      redirect_to post_path(@post), notice: 'Comment added.'
    else
      render 'posts/show'
    end
  end

  def destroy
    comment = Comment.find params[:id]
    post_id = comment.post_id
    comment.destroy
    redirect_to post_path(post_id), notice: 'Comment deleted.'
  end

  private

  def comment_params
    params.require(:comment).permit(:author, :text)
  end

end
