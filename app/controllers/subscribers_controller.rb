class SubscribersController < ApplicationController
  load_and_authorize_resource only: :index

  def index
    @subscribers = Subscriber.all
  end

  def create
    @subscriber = Subscriber.new subscriber_params
    if @subscriber.save
      @message = "Email #{@subscriber.email} subsribed"
    else
      @message = "#{@subscriber.email} #{@subscriber.errors.messages.values.join(", ")}"
    end
  end

  def unsubscribe
    subscriber = Subscriber.find_by_token params[:id]
    if subscriber
      @email = subscriber.email
      subscriber.destroy
    end
  end

  private

  def subscriber_params
    params.require(:subscriber).permit(:email)
  end

end
