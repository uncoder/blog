class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  load_and_authorize_resource

  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new post_params
    @post.user = current_user
    if @post.save
      redirect_to posts_path, notice: 'Post successfully created.'
    else
      render :new
    end
  end

  def edit
    @post = Post.friendly.find params[:id]
  end

  def update
    @post = Post.friendly.find params[:id]
    if @post.update_attributes post_params
      redirect_to posts_path, notice: 'Post saved.'
    else
      render :edit
    end
  end

  def destroy
    @post = Post.friendly.find params[:id]
    @post.destroy
    redirect_to posts_path, notice: 'Post deleted.'
  end

  def show
    @post = Post.friendly.find params[:id]
    @comment = @post.comments.build
  end

  private

  def post_params
    params.require(:post).permit(:title, :body)
  end
end
