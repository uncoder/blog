pagedown_init = ->
  $("textarea.wmd-input").each (i, input) ->
    attr = undefined
    converter = undefined
    editor = undefined
    help = undefined
    attr = $(input).attr("id").split("wmd-input")[1]
    converter = new Markdown.Converter()
    Markdown.Extra.init converter
    help =
      handler: ->
        window.open "http://daringfireball.net/projects/markdown/syntax"
        false

      title: "Markdown Editing Help"

    editor = new Markdown.Editor(converter, attr, help)
    editor.run()

$(document).ready pagedown_init
$(document).on 'page:load', pagedown_init