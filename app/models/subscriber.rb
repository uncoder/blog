class Subscriber < ActiveRecord::Base
  has_secure_token :token

  validates :email, presence: true, uniqueness: true, email: true
  
end
