class Post < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :user
  has_many :comments, dependent: :delete_all

  validates :title, presence: true
  validates :body, presence: true
  validates :user, presence: true

  after_create :send_emails

  default_scope do
    order(id: :desc)
  end

  def send_emails
    PostMailer.post_mail(self).deliver_later
  end

end
