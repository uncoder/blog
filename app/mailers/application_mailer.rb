class ApplicationMailer < ActionMailer::Base
  default from: "news@blog.com"
  layout 'mailer'
end
