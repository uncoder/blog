class PostMailer < ApplicationMailer

  def post_mail(post)
    @post = post
    Subscriber.all.reload.each do |subscriber|
      @token = subscriber.token
      mail(to: subscriber.email, subject: 'New article!')
    end
  end

end
