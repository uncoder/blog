Rails.application.routes.draw do
  devise_for :users

  root 'posts#index'

  resources :posts do
    resources :comments, only: [:create, :destroy]
  end

  resources :subscribers, only: [:create, :index] do
    get :unsubscribe, on: :member
  end
  
end
